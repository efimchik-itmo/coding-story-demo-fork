package com.epam.csdemo;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SortingTest {

    @Test
    void testRegularCase() {
        //arrange
        int[] array = {6, 32, 9, 2, 6, 4};
        Sorting sorting = new Sorting();

        //act
        sorting.sort(array);

        //assert
        assertArrayEquals(new int[]{2, 4, 6, 6, 9, 32}, array);
    }

    @Test
    void testNullCase() {
        //arrange
        Sorting sorting = new Sorting();

        //act & assert
        assertThrows( IllegalArgumentException.class, () -> sorting.sort(null));// fails!!!
    }
}